# jutilities 
[![pipeline status](https://gitlab.com/_utilities_/jutilities/badges/master/pipeline.svg)](https://gitlab.com/_utilities_/jutilities/-/commits/master)
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=_utilities__jutilities&metric=alert_status)](https://sonarcloud.io/summary/new_code?id=_utilities__jutilities)

`jutilities` is a bunch of useful Java libraries.

Currently, contains the following elements:
* `cqrs` - [see more](cqrs/ReadMe.md)
    
## License
[Apache License 2.0](LICENSE)