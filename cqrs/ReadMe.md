# CQRS

## Description
This module contains following elements:
* `cqrs-core` - Base classes with basic implementation.
* `cqrs-spring-boot` - Extensions classes for Spring Framework.
* `cqrs-spring-boot-autoconfigure` - Autoconfiguration classes for Spring Boot.
* `cqrs-spring-boot-starter` - It contains all required dependencies for Spring Boot Starter.

## How to use it
