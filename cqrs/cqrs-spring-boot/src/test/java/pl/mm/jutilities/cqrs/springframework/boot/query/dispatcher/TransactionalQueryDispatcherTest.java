package pl.mm.jutilities.cqrs.springframework.boot.query.dispatcher;

import org.springframework.transaction.annotation.Transactional;
import org.testng.Assert;
import org.testng.annotations.Test;
import pl.mm.jutilities.cqrs.query.dispatcher.QueryDispatcher;

import java.util.ArrayList;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

public class TransactionalQueryDispatcherTest {

    @Test
    public void testDefaultConstructor() {
        QueryDispatcher queryDispatcher = new TransactionalQueryDispatcher();
        assertNotNull(queryDispatcher);
        assertEquals(queryDispatcher.getClass(), TransactionalQueryDispatcher.class);
    }

    @Test
    public void testCreateInstance() {
        QueryDispatcher queryDispatcher = new TransactionalQueryDispatcher(new ArrayList<>());
        assertNotNull(queryDispatcher);
    }

    @Test
    public void testThatHasTransactionalAndIsReadOnly() {
        QueryDispatcher queryDispatcher = new TransactionalQueryDispatcher(new ArrayList<>());
        Transactional annotation = queryDispatcher.getClass().getAnnotation(Transactional.class);
        assertNotNull(annotation);
        Assert.assertTrue(annotation.readOnly());
    }

}