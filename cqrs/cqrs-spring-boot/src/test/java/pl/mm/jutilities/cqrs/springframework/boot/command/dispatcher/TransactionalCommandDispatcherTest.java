package pl.mm.jutilities.cqrs.springframework.boot.command.dispatcher;

import org.springframework.transaction.annotation.Transactional;
import org.testng.Assert;
import org.testng.annotations.Test;
import pl.mm.jutilities.cqrs.command.dispatcher.CommandDispatcher;

import java.util.ArrayList;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

public class TransactionalCommandDispatcherTest {

    @Test
    public void testDefaultConstructor() {
        CommandDispatcher commandDispatcher = new TransactionalCommandDispatcher();
        assertNotNull(commandDispatcher);
        assertEquals(commandDispatcher.getClass(), TransactionalCommandDispatcher.class);
    }

    @Test
    public void testCreateInstance() {
        CommandDispatcher commandDispatcher = new TransactionalCommandDispatcher(new ArrayList<>());
        assertNotNull(commandDispatcher);
    }

    @Test
    public void testThatHasTransactionalAndIsNotReadOnly() {
        CommandDispatcher commandDispatcher = new TransactionalCommandDispatcher(new ArrayList<>());
        Transactional annotation = commandDispatcher.getClass().getAnnotation(Transactional.class);
        assertNotNull(annotation);
        Assert.assertFalse(annotation.readOnly());
    }

}