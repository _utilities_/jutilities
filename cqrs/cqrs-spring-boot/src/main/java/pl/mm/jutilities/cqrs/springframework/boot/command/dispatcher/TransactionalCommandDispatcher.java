package pl.mm.jutilities.cqrs.springframework.boot.command.dispatcher;

import org.springframework.transaction.annotation.Transactional;
import pl.mm.jutilities.cqrs.command.Command;
import pl.mm.jutilities.cqrs.command.dispatcher.DefaultCommandDispatcher;
import pl.mm.jutilities.cqrs.command.CommandHandler;
import pl.mm.jutilities.cqrs.command.CommandResult;

import java.util.Collection;

/**
 * Command dispatcher with spring {@link Transactional} annotation.
 *
 * @author Mariusz Marciniuk
 * @see Command
 * @see CommandResult
 * @since 0.0.1
 */
@Transactional
public class TransactionalCommandDispatcher extends DefaultCommandDispatcher {

    /**
     * Default constructor.
     */
    public TransactionalCommandDispatcher() {
        super();
    }

    /**
     * Constructor with parameter.
     *
     * @param commandHandlers Command handlers
     */
    public TransactionalCommandDispatcher(Collection<CommandHandler<? extends Command, ? extends CommandResult>> commandHandlers) {
        super(commandHandlers);
    }

    @Override
    public <T> T execute(Command command, Class<T> resultType) {
        return super.execute(command, resultType);
    }
}
