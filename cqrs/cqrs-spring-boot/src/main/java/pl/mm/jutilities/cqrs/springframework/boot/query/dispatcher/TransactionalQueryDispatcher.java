package pl.mm.jutilities.cqrs.springframework.boot.query.dispatcher;

import org.springframework.transaction.annotation.Transactional;
import pl.mm.jutilities.cqrs.query.Query;
import pl.mm.jutilities.cqrs.query.QueryHandler;
import pl.mm.jutilities.cqrs.query.dispatcher.DefaultQueryDispatcher;

import java.util.Collection;

/**
 * Query dispatcher with spring {@link Transactional} annotation set to ready only.
 *
 * @author Mariusz Marciniuk
 * @see DefaultQueryDispatcher
 * @since 0.0.1
 */
@Transactional(readOnly = true)
public class TransactionalQueryDispatcher extends DefaultQueryDispatcher {

    /**
     * Default constructor.
     */
    public TransactionalQueryDispatcher() {
        super();
    }

    /**
     * Constructor with parameter.
     *
     * @param queryHandlers Query handlers
     */
    public TransactionalQueryDispatcher(Collection<QueryHandler<?, ?>> queryHandlers) {
        super(queryHandlers);
    }

    @Override
    public <T> T execute(Query query, Class<T> resultType) {
        return super.execute(query, resultType);
    }
}
