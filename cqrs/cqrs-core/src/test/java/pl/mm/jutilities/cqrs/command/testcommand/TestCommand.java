package pl.mm.jutilities.cqrs.command.testcommand;

import pl.mm.jutilities.cqrs.command.Command;

public class TestCommand implements Command {

    private final String command;

    public TestCommand(String command) {
        this.command = command;
    }

    public String getCommand() {
        return command;
    }

}