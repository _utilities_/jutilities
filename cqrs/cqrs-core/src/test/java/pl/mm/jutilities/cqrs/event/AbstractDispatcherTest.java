package pl.mm.jutilities.cqrs.event;

import org.testng.Assert;
import org.testng.annotations.Test;
import pl.mm.jutilities.cqrs.event.dispatcher.AbstractDispatcher;
import pl.mm.jutilities.cqrs.event.dispatcher.Dispatcher;
import pl.mm.jutilities.cqrs.event.testevent.TestEvent;
import pl.mm.jutilities.cqrs.event.testevent.TestEventHandler;
import pl.mm.jutilities.cqrs.event.testevent.TestEventResult;
import pl.mm.jutilities.cqrs.event.testevent2.TestEvent2;
import pl.mm.jutilities.cqrs.event.testevent2.TestEventHandler2;
import pl.mm.jutilities.cqrs.event.testevent2.TestEventResult2;
import pl.mm.jutilities.cqrs.exceptions.CqrsRuntimeException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;

public class AbstractDispatcherTest {

    @Test
    public void testCreateInstance() {
        Dispatcher<Event> dispatcher = new AbstractDispatcher<>(new LinkedHashSet<>()) {
        };
        Assert.assertNotNull(dispatcher);
    }

    @Test
    public void testExecute() {
        Collection<EventHandler<?, ?>> eventHandlers = new ArrayList<>() {{
            add(new TestEventHandler());
        }};

        Dispatcher<Event> dispatcher = new AbstractDispatcher<>(eventHandlers) {
        };

        TestEventResult result = dispatcher.execute(new TestEvent("test-event"), TestEventResult.class);
        Assert.assertEquals(result.getResult(), "Result for the event 'test-event'");
    }

    @Test
    public void testDispatcherWithClassesWithoutImplementingInterfacesEventAndEventResult() {
        Collection<EventHandler<?, ?>> eventHandlers = new ArrayList<>() {{
            add(new TestEventHandler2());
        }};

        Dispatcher<Event> dispatcher = new AbstractDispatcher<>(eventHandlers) {
        };

        TestEventResult2 result = dispatcher.execute(new TestEvent2("test-event-2"), TestEventResult2.class);
        Assert.assertTrue(result.getResult().contains("test-event-2"));
    }

    @Test(expectedExceptions = {CqrsRuntimeException.class},
            expectedExceptionsMessageRegExp = "Not found handler for 'pl.mm.jutilities.cqrs.event.testevent.TestEvent'")
    public void testExecuteEventHandlerNotFound() {
        Collection<EventHandler<?, ?>> eventHandlers = new ArrayList<>();
        new AbstractDispatcher<>(eventHandlers) {
        }.execute(new TestEvent("test-event"), TestEventResult.class);
    }

    @Test(expectedExceptions = CqrsRuntimeException.class,
            expectedExceptionsMessageRegExp = "Collection of event handlers is 'null'")
    public void testEventHandlerNull() {
        new AbstractDispatcher<>(null) {
        }.execute(new TestEvent("test-event"), TestEventResult.class);
    }

    @Test
    public void testEetEventTypeFromEventHandler() {
        final TestEventHandler eventHandler = new TestEventHandler();
        Assert.assertEquals(eventHandler.getEventType(), TestEvent.class);
        Assert.assertEquals(eventHandler.getEventResultType(), TestEventResult.class);
    }

    @Test(expectedExceptions = CqrsRuntimeException.class,
            expectedExceptionsMessageRegExp = "Handler for '.*' already exists\\. Cannot add duplicate")
    public void testTryToAddEventHandlerOfTheSameTypeTwice() {
        final List<EventHandler<?, ?>> eventHandlers = new ArrayList<>();
        eventHandlers.add(new TestEventHandler());
        eventHandlers.add(new TestEventHandler());
        final AbstractDispatcher<?, EventHandler<?, ?>> dispatcher = new AbstractDispatcher<>() {
            @Override
            public <T> T execute(Event event, Class<T> resultType) {
                return super.execute(event, resultType);
            }
        };

        eventHandlers.forEach(dispatcher::addHandler);
    }

    @Test
    public void testAddHandler() {
        final AbstractDispatcher<Event, EventHandler<?, ?>> dispatcher = new AbstractDispatcher<>() {
            @Override
            public <T> T execute(Event event, Class<T> resultType) {
                return super.execute(event, resultType);
            }
        };
        dispatcher.addAllHandlers(List.of(new TestEventHandler()));

        final TestEventResult eventResult = dispatcher.execute(new TestEvent("test-event"), TestEventResult.class);
        Assert.assertTrue(eventResult.getResult().contains("test-event"));
    }

}