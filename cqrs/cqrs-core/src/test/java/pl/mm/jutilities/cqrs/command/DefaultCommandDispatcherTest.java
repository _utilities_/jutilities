package pl.mm.jutilities.cqrs.command;

import org.testng.Assert;
import org.testng.annotations.Test;
import pl.mm.jutilities.cqrs.command.dispatcher.CommandDispatcher;
import pl.mm.jutilities.cqrs.command.dispatcher.DefaultCommandDispatcher;
import pl.mm.jutilities.cqrs.command.testcommand.TestCommand;
import pl.mm.jutilities.cqrs.command.testcommand.TestCommandHandler;
import pl.mm.jutilities.cqrs.command.testcommand.TestCommandResult;

import java.util.ArrayList;
import java.util.Collection;

public class DefaultCommandDispatcherTest {

    @Test
    public void testDefaultConstructor() {
        CommandDispatcher commandDispatcher = new DefaultCommandDispatcher();
        Assert.assertNotNull(commandDispatcher);
        Assert.assertEquals(commandDispatcher.getClass(), DefaultCommandDispatcher.class);
    }

    @Test
    public void testCreatingABasicInstance() {
        CommandDispatcher commandDispatcher = new DefaultCommandDispatcher(new ArrayList<>());
        Assert.assertNotNull(commandDispatcher);
    }

    @Test(expectedExceptions = CommandRuntimeException.class,
            expectedExceptionsMessageRegExp = "Not found command handler for command 'pl.mm.jutilities.cqrs.command.testcommand.TestCommand'")
    public void testNoCommandHandlerFound() {
        Collection<CommandHandler<?, ?>> commandHandlers = new ArrayList<>();
        CommandDispatcher commandDispatcher = new DefaultCommandDispatcher(commandHandlers);
        Command command = new TestCommand("Test command");
        commandDispatcher.execute(command, CommandResult.class);
    }

    @Test
    public void testExecute() {
        Collection<CommandHandler<?, ?>> commandHandlers = new ArrayList<>(){{
            add(new TestCommandHandler());
        }};
        CommandDispatcher commandDispatcher = new DefaultCommandDispatcher(commandHandlers);
        Command command = new TestCommand("Test command");
        TestCommandResult result = commandDispatcher.execute(command, TestCommandResult.class);
        Assert.assertEquals(result.getResult(), "Test command result for 'Test command'");
    }

}