package pl.mm.jutilities.cqrs.query.testquery2;

import pl.mm.jutilities.cqrs.query.QueryHandler;

public class TestQuery2Handler implements QueryHandler<TestQuery2, TestQuery2Result> {

    @Override
    public TestQuery2Result execute(TestQuery2 query) {
        return new TestQuery2Result("QueryResult from '" + TestQuery2Handler.class + "' for query '" + query.getQuery2() + "'");
    }

    @Override
    public Class<TestQuery2> getEventType() {
        return TestQuery2.class;
    }

    @Override
    public Class<TestQuery2Result> getEventResultType() {
        return TestQuery2Result.class;
    }

}
