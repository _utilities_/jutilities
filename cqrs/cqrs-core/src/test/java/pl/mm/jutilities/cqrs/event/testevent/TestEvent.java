package pl.mm.jutilities.cqrs.event.testevent;

import pl.mm.jutilities.cqrs.event.Event;

public final class TestEvent implements Event {

    private final String name;

    public TestEvent(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}