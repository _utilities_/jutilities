package pl.mm.jutilities.cqrs.event.testevent2;

import pl.mm.jutilities.cqrs.event.EventResult;

public final class TestEventResult2 implements EventResult {

    private final String result;

    public TestEventResult2(String result) {
        this.result = result;
    }

    public String getResult() {
        return result;
    }
}
