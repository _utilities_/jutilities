package pl.mm.jutilities.cqrs.event.testevent;

import pl.mm.jutilities.cqrs.event.EventHandler;

public final class TestEventHandler implements EventHandler<TestEvent, TestEventResult> {

    @Override
    public TestEventResult execute(TestEvent event) {
        return new TestEventResult("Result for the event '" + event.getName() + "'");
    }

    @Override
    public Class<TestEvent> getEventType() {
        return TestEvent.class;
    }

    @Override
    public Class<TestEventResult> getEventResultType() {
        return TestEventResult.class;
    }

}