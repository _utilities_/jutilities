package pl.mm.jutilities.cqrs.query.testquery2;

import pl.mm.jutilities.cqrs.query.QueryResult;

public class TestQuery2Result implements QueryResult {

    private final String queryResult2;

    public TestQuery2Result(String queryResult2) {
        this.queryResult2 = queryResult2;
    }

    public String getQueryResult2() {
        return queryResult2;
    }

}
