package pl.mm.jutilities.cqrs.command;

import org.testng.annotations.Test;

public class CommandRuntimeExceptionTest {

    @Test(expectedExceptions = CommandRuntimeException.class)
    public void testException() {
        throw new CommandRuntimeException();
    }

    @Test(expectedExceptions = CommandRuntimeException.class, expectedExceptionsMessageRegExp = "CommandRuntimeException message")
    public void testExceptionWithMessage() {
        throw new CommandRuntimeException("CommandRuntimeException message");
    }

    @Test(expectedExceptions = CommandRuntimeException.class, expectedExceptionsMessageRegExp = "CommandRuntimeException message")
    public void testExceptionWithMessageAndThrowable() {
        throw new CommandRuntimeException("CommandRuntimeException message", new Throwable());
    }

    @Test(expectedExceptions = CommandRuntimeException.class, expectedExceptionsMessageRegExp = "java.lang.Throwable: CommandRuntimeException message")
    public void testExceptionWithThrowable() {
        throw new CommandRuntimeException(new Throwable("CommandRuntimeException message"));
    }

}