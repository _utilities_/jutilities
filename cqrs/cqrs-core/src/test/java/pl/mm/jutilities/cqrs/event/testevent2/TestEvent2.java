package pl.mm.jutilities.cqrs.event.testevent2;

import pl.mm.jutilities.cqrs.event.Event;

public final class TestEvent2 implements Event {

    private final String input;

    public TestEvent2(String input) {
        this.input = input;
    }

    public String getInput() {
        return input;
    }
}
