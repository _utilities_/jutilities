package pl.mm.jutilities.cqrs.query;

import org.testng.Assert;
import org.testng.annotations.Test;
import pl.mm.jutilities.cqrs.query.dispatcher.DefaultQueryDispatcher;
import pl.mm.jutilities.cqrs.query.dispatcher.QueryDispatcher;
import pl.mm.jutilities.cqrs.query.testquery.TestQuery;
import pl.mm.jutilities.cqrs.query.testquery.TestQueryHandler;
import pl.mm.jutilities.cqrs.query.testquery.TestQueryResult;
import pl.mm.jutilities.cqrs.query.testquery2.TestQuery2;
import pl.mm.jutilities.cqrs.query.testquery2.TestQuery2Handler;
import pl.mm.jutilities.cqrs.query.testquery2.TestQuery2Result;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Set;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

public class DefaultQueryDispatcherTest {

    @Test
    public void testDefaultConstructor() {
        QueryDispatcher queryDispatcher = new DefaultQueryDispatcher();
        assertNotNull(queryDispatcher);
        assertEquals(queryDispatcher.getClass(), DefaultQueryDispatcher.class);
    }

    @Test
    public void testCreatingABasicInstance() {
        Set<QueryHandler<?, ?>> queryHandlerSet = new LinkedHashSet<>();
        QueryDispatcher queryDispatcher = new DefaultQueryDispatcher(queryHandlerSet);
        assertNotNull(queryDispatcher);
    }

    @Test(expectedExceptions = QueryRuntimeException.class,
            expectedExceptionsMessageRegExp = "Not found query handler for query 'pl.mm.jutilities.cqrs.query.testquery.TestQuery'")
    public void testNoQueryHandlers() {
        Set<QueryHandler<?, ?>> queryHandlerSet = new LinkedHashSet<>();
        QueryDispatcher queryDispatcher = new DefaultQueryDispatcher(queryHandlerSet);
        Query query = new TestQuery("SomeTestQuery");

        queryDispatcher.execute(query, TestQueryResult.class);
    }

    @Test(expectedExceptions = QueryRuntimeException.class, enabled = false,
            expectedExceptionsMessageRegExp = "Not found query handler for query 'pl.mm.jutilities.cqrs.query.TestQuery'")
    public void testNoQueryHandlersDuplicatedQueryHandlers() {
        Collection<QueryHandler<?, ?>> queryHandlers = new LinkedHashSet<>() {{
            add(new TestQueryHandler());
            add(new TestQueryHandler());
        }};

        new DefaultQueryDispatcher(queryHandlers);
    }

    @Test
    public void testExecute() {
        Set<QueryHandler<?, ?>> queryHandlerSet = new LinkedHashSet<>() {{
            add(new TestQueryHandler());
        }};
        QueryDispatcher queryDispatcher = new DefaultQueryDispatcher(queryHandlerSet);
        Query query = new TestQuery("SomeTestQuery");

        TestQueryResult queryResult = queryDispatcher.execute(query, TestQueryResult.class);
        Assert.assertNotNull(queryResult);
        Assert.assertNotNull(queryResult.getQueryResult());
    }

    @Test
    public void testMoreThanOneQuery() {
        Set<QueryHandler<?, ?>> queryHandlerSet = new LinkedHashSet<>() {{
            add(new TestQueryHandler());
            add(new TestQuery2Handler());
        }};
        QueryDispatcher queryDispatcher = new DefaultQueryDispatcher(queryHandlerSet);
        final String queryParam = "SomeTestQuery2";
        Query query = new TestQuery2(queryParam);

        TestQuery2Result queryResult = queryDispatcher.execute(query, TestQuery2Result.class);
        Assert.assertNotNull(queryResult);
        Assert.assertNotNull(queryResult.getQueryResult2());
        Assert.assertTrue(queryResult.getQueryResult2().contains(queryParam));
        Assert.assertEquals(queryResult.getClass(), TestQuery2Result.class);
    }

}