package pl.mm.jutilities.cqrs.query.testquery2;

import pl.mm.jutilities.cqrs.query.Query;

public final class TestQuery2 implements Query {

    private final String query2;

    public TestQuery2(String query2) {
        this.query2 = query2;
    }

    public String getQuery2() {
        return query2;
    }
}
