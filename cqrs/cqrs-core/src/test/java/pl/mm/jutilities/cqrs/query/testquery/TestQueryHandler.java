package pl.mm.jutilities.cqrs.query.testquery;

import pl.mm.jutilities.cqrs.query.QueryHandler;

public final class TestQueryHandler implements QueryHandler<TestQuery, TestQueryResult> {

    @Override
    public TestQueryResult execute(TestQuery query) {
        return new TestQueryResult("QueryResult from '" + TestQueryHandler.class + "' for query '" + query.getQuery() + "'");
    }

    @Override
    public Class<TestQuery> getEventType() {
        return TestQuery.class;
    }

    @Override
    public Class<TestQueryResult> getEventResultType() {
        return TestQueryResult.class;
    }

}