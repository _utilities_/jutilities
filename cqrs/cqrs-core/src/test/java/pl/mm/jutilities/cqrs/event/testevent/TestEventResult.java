package pl.mm.jutilities.cqrs.event.testevent;

import pl.mm.jutilities.cqrs.event.EventResult;

public final class TestEventResult implements EventResult {

    private final String result;

    public TestEventResult(String result) {
        this.result = result;
    }

    public String getResult() {
        return result;
    }
}