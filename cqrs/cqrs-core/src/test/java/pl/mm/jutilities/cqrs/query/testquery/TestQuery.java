package pl.mm.jutilities.cqrs.query.testquery;

import pl.mm.jutilities.cqrs.query.Query;

public final class TestQuery implements Query {

    private final String query;

    public TestQuery(String query) {
        this.query = query;
    }

    public String getQuery() {
        return query;
    }

}
