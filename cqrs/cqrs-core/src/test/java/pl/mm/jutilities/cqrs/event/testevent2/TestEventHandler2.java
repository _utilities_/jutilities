package pl.mm.jutilities.cqrs.event.testevent2;

import pl.mm.jutilities.cqrs.event.EventHandler;

public final class TestEventHandler2 implements EventHandler<TestEvent2, TestEventResult2> {
    @Override
    public TestEventResult2 execute(TestEvent2 event) {
        return new TestEventResult2("Result from '" + TestEventResult2.class + "' = '" + event.getInput() + "'");
    }

    @Override
    public Class<TestEvent2> getEventType() {
        return TestEvent2.class;
    }

    @Override
    public Class<TestEventResult2> getEventResultType() {
        return TestEventResult2.class;
    }
}
