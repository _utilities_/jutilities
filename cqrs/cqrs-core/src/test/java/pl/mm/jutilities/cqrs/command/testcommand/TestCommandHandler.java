package pl.mm.jutilities.cqrs.command.testcommand;

import pl.mm.jutilities.cqrs.command.CommandHandler;

public class TestCommandHandler implements CommandHandler<TestCommand, TestCommandResult> {

    @Override
    public TestCommandResult execute(TestCommand command) {
        return new TestCommandResult("Test command result for '" + command.getCommand() + "'");
    }

    @Override
    public Class<TestCommand> getEventType() {
        return TestCommand.class;
    }

    @Override
    public Class<TestCommandResult> getEventResultType() {
        return TestCommandResult.class;
    }

}