package pl.mm.jutilities.cqrs.query.testquery;

import pl.mm.jutilities.cqrs.query.QueryResult;

public final class TestQueryResult implements QueryResult {

    private final String queryResult;

    public TestQueryResult(String queryResult) {
        this.queryResult = queryResult;
    }

    public String getQueryResult() {
        return queryResult;
    }

}