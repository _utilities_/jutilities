package pl.mm.jutilities.cqrs.exceptions;

import org.testng.annotations.Test;

public class CqrsRuntimeExceptionTest {

    @Test(expectedExceptions = CqrsRuntimeException.class)
    public void testExecution() {
        throw new CqrsRuntimeException();
    }

    @Test(expectedExceptions = CqrsRuntimeException.class, expectedExceptionsMessageRegExp = "CqrsRuntimeException message")
    public void testExecutionWithMessage() {
        throw new CqrsRuntimeException("CqrsRuntimeException message");
    }

    @Test(expectedExceptions = CqrsRuntimeException.class, expectedExceptionsMessageRegExp = "CqrsRuntimeException message")
    public void testExecutionWithMessageAndThrowable() {
        throw new CqrsRuntimeException("CqrsRuntimeException message", new Throwable());
    }

    @Test(expectedExceptions = CqrsRuntimeException.class, expectedExceptionsMessageRegExp = "java.lang.Throwable: CqrsRuntimeException message from Throwable")
    public void testExecutionWithThrowable() {
        throw new CqrsRuntimeException(new Throwable("CqrsRuntimeException message from Throwable"));
    }

}