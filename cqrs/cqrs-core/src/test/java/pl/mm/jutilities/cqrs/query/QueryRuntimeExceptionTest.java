package pl.mm.jutilities.cqrs.query;

import org.testng.annotations.Test;

public class QueryRuntimeExceptionTest {

    @Test(expectedExceptions = QueryRuntimeException.class)
    public void testExecution() {
        throw new QueryRuntimeException();
    }

    @Test(expectedExceptions = QueryRuntimeException.class, expectedExceptionsMessageRegExp = "QueryRuntimeException message")
    public void testExecutionWithMessage() {
        throw new QueryRuntimeException("QueryRuntimeException message");
    }

    @Test(expectedExceptions = QueryRuntimeException.class, expectedExceptionsMessageRegExp = "QueryRuntimeException message")
    public void testExecutionWithMessageAndThrowable() {
        throw new QueryRuntimeException("QueryRuntimeException message", new Throwable());
    }

    @Test(expectedExceptions = QueryRuntimeException.class, expectedExceptionsMessageRegExp = "java.lang.Throwable: QueryRuntimeException message from Throwable")
    public void testExecutionWithThrowable() {
        throw new QueryRuntimeException(new Throwable("QueryRuntimeException message from Throwable"));
    }    

}