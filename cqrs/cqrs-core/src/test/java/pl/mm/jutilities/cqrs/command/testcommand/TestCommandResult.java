package pl.mm.jutilities.cqrs.command.testcommand;

import pl.mm.jutilities.cqrs.command.CommandResult;

public class TestCommandResult implements CommandResult {

    private final String result;

    public TestCommandResult(String result) {
        this.result = result;
    }

    public String getResult() {
        return result;
    }
}