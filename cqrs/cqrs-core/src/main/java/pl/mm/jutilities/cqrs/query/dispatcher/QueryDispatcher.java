package pl.mm.jutilities.cqrs.query.dispatcher;

import pl.mm.jutilities.cqrs.event.dispatcher.Dispatcher;
import pl.mm.jutilities.cqrs.query.Query;

/**
 * Query dispatcher interface. The purpose of this interface is to find a proper query handler for the received query.
 * Then the query will be passed found query handler, which will execute the query, and then the result will be returned.
 *
 * @author Mariusz Marciniuk
 * @see Dispatcher
 * @since 0.0.1
 */
public interface QueryDispatcher extends Dispatcher<Query> {

    /**
     * It will search for a proper query handler that can handle the received query. Then the query will be passed to
     * be executed by the query handler, which will execute the query and then will be cast to provided class type.
     *
     * @param query      Query
     * @param resultType Result to which it will be cast.
     * @param <T>        Result type
     * @return Result cast to provided class type.
     */
    @Override
    <T> T execute(Query query, Class<T> resultType);

}
