package pl.mm.jutilities.cqrs.query;

import pl.mm.jutilities.cqrs.event.EventHandler;

/**
 * Query handler interface.
 * <br>
 * <br>
 * One query should have exactly one query handler.
 *
 * @param <Q> Type of query
 * @param <R> Type of query result
 * @author Mariusz Marciniuk
 * @see EventHandler
 * @since 0.0.1
 */
public interface QueryHandler<Q extends Query, R extends QueryResult> extends EventHandler<Q, R> {

    /**
     * Executes the query and return the result.
     *
     * @param query Query
     * @return Query result
     */
    R execute(Q query);

}
