package pl.mm.jutilities.cqrs.command;

import pl.mm.jutilities.cqrs.exceptions.CqrsRuntimeException;

/**
 * CommandRuntimeException
 *
 * @author Mariusz Marciniuk
 * @see CqrsRuntimeException
 * @since 0.0.1
 */
public class CommandRuntimeException extends CqrsRuntimeException {

    /**
     * See documentation of {@link CqrsRuntimeException#CqrsRuntimeException()}
     */
    public CommandRuntimeException() {
    }

    /**
     * See documentation of {@link CqrsRuntimeException#CqrsRuntimeException(String)}
     */
    public CommandRuntimeException(String message) {
        super(message);
    }

    /**
     * See documentation of {@link CqrsRuntimeException#CqrsRuntimeException(String, Throwable)}
     */
    public CommandRuntimeException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * See documentation of {@link CqrsRuntimeException#CqrsRuntimeException(Throwable)}
     */
    public CommandRuntimeException(Throwable cause) {
        super(cause);
    }

}
