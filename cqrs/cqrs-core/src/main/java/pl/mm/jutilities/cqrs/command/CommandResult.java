package pl.mm.jutilities.cqrs.command;

import pl.mm.jutilities.cqrs.event.EventResult;

/**
 * Interface which marks a class as a command result.
 *
 * @author Mariusz Marciniuk
 * @see EventResult
 * @see pl.mm.jutilities.cqrs.command.Command
 * @since 0.0.1
 */
public interface CommandResult extends EventResult {
}
