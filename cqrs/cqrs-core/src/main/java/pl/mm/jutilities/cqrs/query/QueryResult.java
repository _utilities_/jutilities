package pl.mm.jutilities.cqrs.query;

import pl.mm.jutilities.cqrs.event.EventResult;

/**
 * Interface which marks a class as a query result.
 *
 * @author Mariusz Marciniuk
 * @see EventResult
 * @see pl.mm.jutilities.cqrs.query.Query
 * @since 0.0.1
 */
public interface QueryResult extends EventResult {
}
