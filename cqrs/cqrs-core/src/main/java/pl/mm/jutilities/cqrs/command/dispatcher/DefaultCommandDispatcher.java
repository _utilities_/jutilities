package pl.mm.jutilities.cqrs.command.dispatcher;

import pl.mm.jutilities.cqrs.command.Command;
import pl.mm.jutilities.cqrs.command.CommandHandler;
import pl.mm.jutilities.cqrs.command.CommandResult;
import pl.mm.jutilities.cqrs.command.CommandRuntimeException;
import pl.mm.jutilities.cqrs.event.EventHandler;
import pl.mm.jutilities.cqrs.event.dispatcher.AbstractDispatcher;
import pl.mm.jutilities.cqrs.exceptions.CqrsRuntimeException;

import java.util.Collection;

/**
 * Default command dispatcher.
 *
 * @author Mariusz Marciniuk
 * @see Command
 * @see CommandResult
 * @see CommandDispatcher
 * @see AbstractDispatcher
 * @since 0.0.1
 */
public class DefaultCommandDispatcher extends AbstractDispatcher<Command, CommandHandler<? extends Command, ? extends CommandResult>>
        implements CommandDispatcher {

    /**
     * Default constructor.
     */
    public DefaultCommandDispatcher() {
        super();
    }

    /**
     * Constructor with parameter.
     *
     * @param commandHandlers Command handlers
     */
    public DefaultCommandDispatcher(Collection<CommandHandler<? extends Command, ? extends CommandResult>> commandHandlers) {
        super(commandHandlers);
    }

    /**
     * This method will search for a command handler for the received command. It can throw {@link CommandRuntimeException}
     * when no command handler has been found for the received command.
     *
     * @param command Command
     * @return Found command handler for received query.
     */
    @Override
    protected EventHandler<?, ?> findEventHandlerForEvent(Command command) {
        try {
            return super.findEventHandlerForEvent(command);
        } catch (CqrsRuntimeException exception) {
            throw new CommandRuntimeException("Not found command handler for command '" + command.getClass().getName() + "'");
        }
    }
}
