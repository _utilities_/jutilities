package pl.mm.jutilities.cqrs.event;

/**
 * Event result interface.
 *
 * @author Mariusz Marciniuk
 * @see Event
 * @since 0.0.1
 */
public interface EventResult {
}
