package pl.mm.jutilities.cqrs.event.dispatcher;

import pl.mm.jutilities.cqrs.event.Event;

/**
 * Dispatcher interface. The purpose of this interface is to find a proper event handler for the received event.
 * Then the event will be passed found event handler, which will execute the event, and then the result will be returned.
 *
 * @param <E> Event
 * @author Mariusz Maricniuk
 * @since 0.0.1
 */
public interface Dispatcher<E extends Event> {

    /**
     * It will search for a proper event handler that can handle the received event. Then the event will be passed to
     * be executed by the event handler, which will execute the event and then will be cast to provided class type.
     *
     * @param event      Event
     * @param resultType Result to which it will be cast.
     * @param <T>        Result type
     * @return Result cast to provided class type.
     */
    <T> T execute(E event, Class<T> resultType);

}
