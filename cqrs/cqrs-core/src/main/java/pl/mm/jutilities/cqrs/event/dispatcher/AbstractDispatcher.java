package pl.mm.jutilities.cqrs.event.dispatcher;

import pl.mm.jutilities.cqrs.event.Event;
import pl.mm.jutilities.cqrs.event.EventHandler;
import pl.mm.jutilities.cqrs.exceptions.CqrsRuntimeException;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Abstract event dispatcher with basic implementation.
 *
 * @param <E> Event | {@link Event}
 * @param <H> EventHandler | {@link EventHandler}
 * @author Mariusz Marciniuk
 * @see Event
 * @see EventHandler
 * @see Dispatcher
 * @since 0.0.1
 */
public abstract class AbstractDispatcher<E extends Event, H extends EventHandler<?, ?>> implements Dispatcher<E> {

    /**
     * Collection of event handlers.
     */
    protected final Map<String, H> eventHandlers;

    /**
     * Default constructor.
     */
    protected AbstractDispatcher() {
        eventHandlers = new LinkedHashMap<>();
    }

    /**
     * Constructor with parameter.
     *
     * @param eventHandlers Event handlers.
     */
    protected AbstractDispatcher(Collection<H> eventHandlers) {
        this();
        if (eventHandlers == null) {
            throw new CqrsRuntimeException("Collection of event handlers is 'null'");
        }
        eventHandlers.forEach(this::addHandler);
    }

    @Override
    public <T> T execute(E event, Class<T> resultType) {
        //noinspection rawtypes
        final EventHandler eventHandler = findEventHandlerForEvent(event);
        //noinspection unchecked
        return (T) eventHandler.execute(event);
    }

    /**
     * This method will search for an event handler for the received event. It can throw {@link CqrsRuntimeException}
     * when no event handler has been found for the received event.
     *
     * @param event Event
     * @return Found event handler for received event.
     */
    protected EventHandler<?, ?> findEventHandlerForEvent(E event) {
        final H eventHandler = eventHandlers.get(event.getClass().getTypeName());
        if (eventHandler != null) {
            return eventHandler;
        }
        throw new CqrsRuntimeException("Not found handler for '" + event.getClass().getName() + "'");
    }

    /**
     * Adds handler to the event handlers. If duplicate exists it will trow an exception.
     *
     * @param handler Handler
     * @throws CqrsRuntimeException Exception
     */
    public void addHandler(final H handler) {
        final Class<?> eventType = handler.getEventType();
        final H eventHandler = eventHandlers.putIfAbsent(eventType.getTypeName(), handler);
        if (eventHandler != null) {
            throw new CqrsRuntimeException("Handler for '" + eventType.getTypeName() + "' already exists. Cannot add duplicate");
        }
    }

    /**
     * Adds all handlers from the collection.
     *
     * @param handlers Handlers
     */
    public void addAllHandlers(final Collection<H> handlers) {
        handlers.forEach(this::addHandler);
    }

}
