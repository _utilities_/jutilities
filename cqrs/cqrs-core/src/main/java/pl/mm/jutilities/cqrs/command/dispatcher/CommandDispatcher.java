package pl.mm.jutilities.cqrs.command.dispatcher;

import pl.mm.jutilities.cqrs.command.Command;
import pl.mm.jutilities.cqrs.event.dispatcher.Dispatcher;

/**
 * Command dispatcher interface. The purpose of this interface is to find a proper command handler for the received command.
 * Then the command will be passed found command handler, which will execute the command, and then the result will be returned.
 *
 * @author Mariusz Marciniuk
 * @see Dispatcher
 * @since 0.0.1
 */
public interface CommandDispatcher extends Dispatcher<Command> {

    /**
     * It will search for a proper command handler that can handle the received command. Then the command will be passed to
     * be executed by the command handler, which will execute the command and then will be cast to provided class type.
     *
     * @param command    Command
     * @param resultType Result to which it will be cast.
     * @param <T>        Result type
     * @return Result cast to provided class type.
     */
    @Override
    <T> T execute(Command command, Class<T> resultType);
}
