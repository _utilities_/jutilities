package pl.mm.jutilities.cqrs.query;

import pl.mm.jutilities.cqrs.event.Event;

/**
 * Interface which marks a class as a query.
 *
 * @author Mariusz Marciniuk
 * @since 0.0.1
 */
public interface Query extends Event {
}
