package pl.mm.jutilities.cqrs.query;

import pl.mm.jutilities.cqrs.exceptions.CqrsRuntimeException;

/**
 * QueryRuntimeException
 *
 * @author Mariusz Marciniuk
 * @see CqrsRuntimeException
 * @since 0.0.1
 */
public class QueryRuntimeException extends CqrsRuntimeException {

    /**
     * See documentation of {@link CqrsRuntimeException#CqrsRuntimeException()}
     */
    public QueryRuntimeException() {
    }

    /**
     * See documentation of {@link CqrsRuntimeException#CqrsRuntimeException(String)}
     */
    public QueryRuntimeException(String message) {
        super(message);
    }

    /**
     * See documentation of {@link CqrsRuntimeException#CqrsRuntimeException(String, Throwable)}
     */
    public QueryRuntimeException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * See documentation of {@link CqrsRuntimeException#CqrsRuntimeException(Throwable)}
     */
    public QueryRuntimeException(Throwable cause) {
        super(cause);
    }

}
