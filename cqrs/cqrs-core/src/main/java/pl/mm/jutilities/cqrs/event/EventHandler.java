package pl.mm.jutilities.cqrs.event;

/**
 * Event handler interface.
 * <br>
 * <br>
 * One event should have exactly one event handler.
 *
 * @param <E> Event type
 * @param <R> Event result type
 * @author Mariusz Marciniuk
 * @since 0.0.1
 */
public interface EventHandler<E extends Event, R extends EventResult> {

    /**
     * Executes the event and return the result.
     *
     * @param event Event
     * @return Event result
     */
    R execute(E event);

    /**
     * This method return actual type of event.
     *
     * @return Actual type of event.
     */
    Class<E> getEventType();

    /**
     * This method return actual type of event result.
     *
     * @return Actual type of event result.
     */
    Class<R> getEventResultType();

}
