package pl.mm.jutilities.cqrs.command;

import pl.mm.jutilities.cqrs.event.EventHandler;

/**
 * Command handler interface.
 * <br>
 * <br>
 * One command should have exactly one command handler.
 *
 * @param <C> Type of command
 * @param <R> Type of query result
 * @author Mariusz Marciniuk
 * @see EventHandler
 * @since 0.0.1
 */
public interface CommandHandler<C extends Command, R extends CommandResult> extends EventHandler<C, R> {

    /**
     * Executes command event.
     *
     * @param command Command
     * @return command result
     */
    R execute(C command);

}
