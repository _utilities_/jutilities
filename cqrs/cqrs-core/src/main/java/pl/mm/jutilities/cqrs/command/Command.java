package pl.mm.jutilities.cqrs.command;

import pl.mm.jutilities.cqrs.event.Event;

/**
 * Interface which marks a class as a command.
 *
 * @author Mariusz Marciniuk
 * @since 0.0.1
 */
public interface Command extends Event {
}
