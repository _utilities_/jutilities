package pl.mm.jutilities.cqrs.query.dispatcher;

import pl.mm.jutilities.cqrs.event.EventHandler;
import pl.mm.jutilities.cqrs.event.dispatcher.AbstractDispatcher;
import pl.mm.jutilities.cqrs.exceptions.CqrsRuntimeException;
import pl.mm.jutilities.cqrs.query.Query;
import pl.mm.jutilities.cqrs.query.QueryHandler;
import pl.mm.jutilities.cqrs.query.QueryResult;
import pl.mm.jutilities.cqrs.query.QueryRuntimeException;

import java.util.Collection;

/**
 * Default query dispatcher.
 *
 * @author Mariusz Marciniuk
 * @see Query
 * @see QueryResult
 * @see QueryHandler
 * @see QueryDispatcher
 * @see AbstractDispatcher
 * @since 0.0.1
 */
public class DefaultQueryDispatcher extends AbstractDispatcher<Query, QueryHandler<? extends Query, ? extends QueryResult>>
        implements QueryDispatcher {

    /**
     * Default constructor.
     */
    public DefaultQueryDispatcher() {
        super();
    }

    /**
     * Constructor with parameter.
     *
     * @param queryHandlers Query handlers
     */
    public DefaultQueryDispatcher(Collection<QueryHandler<? extends Query, ? extends QueryResult>> queryHandlers) {
        super(queryHandlers);
    }

    /**
     * This method will search for a query handler for the received query. It can throw {@link QueryRuntimeException}
     * when no query handler has been found for the received query.
     *
     * @param query Query
     * @return Found query handler for received query.
     */
    @Override
    protected EventHandler<?, ?> findEventHandlerForEvent(Query query) {
        try {
            return super.findEventHandlerForEvent(query);
        } catch (CqrsRuntimeException exception) {
            throw new QueryRuntimeException("Not found query handler for query '" + query.getClass().getName() + "'");
        }
    }
}
