package pl.mm.jutilities.cqrs.exceptions;

/**
 * CqrsRuntimeException
 *
 * @author Mariusz Marciniuk
 * @since 0.0.1
 */
public class CqrsRuntimeException extends RuntimeException {

    /**
     * See documentation of {@link RuntimeException#RuntimeException()}
     */
    public CqrsRuntimeException() {
    }

    /**
     * See documentation of {@link RuntimeException#RuntimeException(String)}
     */
    public CqrsRuntimeException(String message) {
        super(message);
    }

    /**
     * See documentation of {@link RuntimeException#RuntimeException(String, Throwable)}
     */
    public CqrsRuntimeException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * See documentation of {@link RuntimeException#RuntimeException(Throwable)}
     */
    public CqrsRuntimeException(Throwable cause) {
        super(cause);
    }

}
