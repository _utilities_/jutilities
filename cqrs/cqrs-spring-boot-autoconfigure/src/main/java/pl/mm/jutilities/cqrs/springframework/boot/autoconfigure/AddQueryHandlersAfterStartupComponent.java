package pl.mm.jutilities.cqrs.springframework.boot.autoconfigure;

import pl.mm.jutilities.cqrs.query.QueryHandler;
import pl.mm.jutilities.cqrs.query.dispatcher.DefaultQueryDispatcher;
import pl.mm.jutilities.cqrs.query.dispatcher.QueryDispatcher;

import java.util.Collection;

public class AddQueryHandlersAfterStartupComponent
        extends AddAbstractHandlersAfterStartupComponent<QueryDispatcher, QueryHandler<?, ?>> {

    public AddQueryHandlersAfterStartupComponent(QueryDispatcher dispatcher, Collection<QueryHandler<?, ?>> handlers) {
        super(dispatcher, handlers);
    }

    @Override
    protected Class<?> getDispatcherClass() {
        return QueryDispatcher.class;
    }

    @Override
    protected void delegatedAddHandlers(QueryDispatcher dispatcher, Collection<QueryHandler<?, ?>> handlers) {
        ((DefaultQueryDispatcher) dispatcher).addAllHandlers(handlers);
    }
}
