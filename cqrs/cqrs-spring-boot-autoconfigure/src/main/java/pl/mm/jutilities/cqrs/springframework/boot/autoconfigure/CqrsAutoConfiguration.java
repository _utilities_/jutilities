package pl.mm.jutilities.cqrs.springframework.boot.autoconfigure;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.Transactional;
import pl.mm.jutilities.cqrs.command.Command;
import pl.mm.jutilities.cqrs.command.CommandHandler;
import pl.mm.jutilities.cqrs.command.CommandResult;
import pl.mm.jutilities.cqrs.command.dispatcher.CommandDispatcher;
import pl.mm.jutilities.cqrs.command.dispatcher.DefaultCommandDispatcher;
import pl.mm.jutilities.cqrs.query.Query;
import pl.mm.jutilities.cqrs.query.QueryHandler;
import pl.mm.jutilities.cqrs.query.QueryResult;
import pl.mm.jutilities.cqrs.query.dispatcher.DefaultQueryDispatcher;
import pl.mm.jutilities.cqrs.query.dispatcher.QueryDispatcher;
import pl.mm.jutilities.cqrs.springframework.boot.command.dispatcher.TransactionalCommandDispatcher;
import pl.mm.jutilities.cqrs.springframework.boot.query.dispatcher.TransactionalQueryDispatcher;

import java.util.Collection;

/**
 * {@link EnableAutoConfiguration Auto-configuration} for {@link QueryDispatcher} and {@link CommandDispatcher}.
 *
 * @author Mariusz Marciniuk
 * @since 0.0.1
 */
@Configuration
public class CqrsAutoConfiguration {

    @ConditionalOnClass(Transactional.class)
    @Bean("queryDispatcher")
    public QueryDispatcher transactionalQueryDispatcher() {
        return new TransactionalQueryDispatcher();
    }

    @ConditionalOnClass(Transactional.class)
    @Bean("commandDispatcher")
    public CommandDispatcher transactionalCommandDispatcher() {
        return new TransactionalCommandDispatcher();
    }

    @ConditionalOnMissingBean
    @Bean("queryDispatcher")
    public QueryDispatcher queryDispatcher() {
        return new DefaultQueryDispatcher();
    }

    @ConditionalOnMissingBean
    @Bean("commandDispatcher")
    public CommandDispatcher commandDispatcher() {
        return new DefaultCommandDispatcher();
    }

    @Autowired
    @Bean
    public AddCommandHandlersAfterStartupComponent addCommandHandlersAfterStartupComponent(CommandDispatcher commandDispatcher,
                                                                                           Collection<CommandHandler<? extends Command, ? extends CommandResult>> commandHandlers) {
        return new AddCommandHandlersAfterStartupComponent(commandDispatcher, commandHandlers);
    }

    @Autowired
    @Bean
    public AddQueryHandlersAfterStartupComponent addQueryHandlersAfterStartupComponent(QueryDispatcher queryDispatcher,
                                                                                       Collection<QueryHandler<? extends Query, ? extends QueryResult>> queryHandlers) {
        return new AddQueryHandlersAfterStartupComponent(queryDispatcher, queryHandlers);
    }

}
