package pl.mm.jutilities.cqrs.springframework.boot.autoconfigure;

import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.core.Ordered;
import pl.mm.jutilities.cqrs.event.EventHandler;
import pl.mm.jutilities.cqrs.event.dispatcher.Dispatcher;

import java.util.Collection;

public abstract class AddAbstractHandlersAfterStartupComponent<D extends Dispatcher<?>, H extends EventHandler<?, ?>>
        implements ApplicationListener<ContextRefreshedEvent>, Ordered {

    protected final D dispatcher;
    protected final Collection<H> handlers;

    protected AddAbstractHandlersAfterStartupComponent(D dispatcher, Collection<H> handlers) {
        this.dispatcher = dispatcher;
        this.handlers = handlers;
    }

    @Override
    public int getOrder() {
        return LOWEST_PRECEDENCE;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        if (getDispatcherClass().isAssignableFrom(dispatcher.getClass())) {
            delegatedAddHandlers(dispatcher, handlers);
        }
    }

    protected abstract Class<?> getDispatcherClass();

    protected abstract void delegatedAddHandlers(D dispatcher, Collection<H> handlers);
}
