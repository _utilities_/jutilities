package pl.mm.jutilities.cqrs.springframework.boot.autoconfigure;

import pl.mm.jutilities.cqrs.command.Command;
import pl.mm.jutilities.cqrs.command.CommandHandler;
import pl.mm.jutilities.cqrs.command.CommandResult;
import pl.mm.jutilities.cqrs.command.dispatcher.CommandDispatcher;
import pl.mm.jutilities.cqrs.command.dispatcher.DefaultCommandDispatcher;

import java.util.Collection;

public class AddCommandHandlersAfterStartupComponent
        extends AddAbstractHandlersAfterStartupComponent<CommandDispatcher, CommandHandler<? extends Command, ? extends CommandResult>> {

    public AddCommandHandlersAfterStartupComponent(CommandDispatcher commandDispatcher,
                                                   Collection<CommandHandler<? extends Command, ? extends CommandResult>> commandHandlers) {
        super(commandDispatcher, commandHandlers);
    }

    @Override
    protected Class<?> getDispatcherClass() {
        return CommandDispatcher.class;
    }

    @Override
    protected void delegatedAddHandlers(CommandDispatcher dispatcher, Collection<CommandHandler<?, ?>> handlers) {
        ((DefaultCommandDispatcher) dispatcher).addAllHandlers(handlers);
    }
}
