CREATE TABLE note
(
    id   BIGINT      NOT NULL PRIMARY KEY AUTO_INCREMENT,
    note VARCHAR(50) NOT NULL
);

CREATE TABLE memo
(
    id   BIGINT      NOT NULL PRIMARY KEY AUTO_INCREMENT,
    memo VARCHAR(50) NOT NULL
);