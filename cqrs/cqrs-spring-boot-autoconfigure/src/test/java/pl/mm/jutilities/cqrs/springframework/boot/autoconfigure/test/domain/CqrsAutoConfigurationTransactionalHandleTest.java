package pl.mm.jutilities.cqrs.springframework.boot.autoconfigure.test.domain;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.ContextConfiguration;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pl.mm.jutilities.cqrs.command.dispatcher.CommandDispatcher;
import pl.mm.jutilities.cqrs.query.dispatcher.QueryDispatcher;
import pl.mm.jutilities.cqrs.springframework.boot.autoconfigure.CqrsAutoConfigurationBaseTest;
import pl.mm.jutilities.cqrs.springframework.boot.autoconfigure.test.domain.cqrs.command.CreateNewNotesCommand;
import pl.mm.jutilities.cqrs.springframework.boot.autoconfigure.test.domain.cqrs.command.newmemoandnote.NewMemoAndNoteCommand;
import pl.mm.jutilities.cqrs.springframework.boot.autoconfigure.test.domain.cqrs.command.newmemoandnote.NewMemoAndNoteCommandResult;
import pl.mm.jutilities.cqrs.springframework.boot.autoconfigure.test.domain.cqrs.command.result.CreateNewNotesCommandResult;
import pl.mm.jutilities.cqrs.springframework.boot.autoconfigure.test.domain.repository.MemoRepository;
import pl.mm.jutilities.cqrs.springframework.boot.autoconfigure.test.domain.repository.NoteRepository;

import java.util.ArrayList;

@ContextConfiguration(classes = CqrsAutoConfigurationTransactionalHandleTestConfiguration.class)
public class CqrsAutoConfigurationTransactionalHandleTest extends CqrsAutoConfigurationBaseTest {

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    @Autowired
    private CommandDispatcher commandDispatcher;
    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    @Autowired
    private QueryDispatcher queryDispatcher;
    @Autowired
    private NoteRepository noteRepository;
    @Autowired
    private MemoRepository memoRepository;

    @BeforeMethod
    public void cleanUpBdBeforeTest() {
        noteRepository.deleteAll();
    }

    @Test
    public void incorrectCommandForNoteAndMemo() {
        try {
            commandDispatcher
                    .execute(new NewMemoAndNoteCommand(null, "Memo"), NewMemoAndNoteCommandResult.class);
        } catch (Exception ignored) {
        }
        final int expectedSizeOfRepository = 0;
        Assert.assertEquals(noteRepository.findAll().size(), expectedSizeOfRepository);
        Assert.assertEquals(memoRepository.findAll().size(), expectedSizeOfRepository);
    }

    @Test
    public void incorrectCommandWithNullNote() {
        try {
            commandDispatcher.execute(new CreateNewNotesCommand(new ArrayList<>(){{
                add("test-note");
                add(null);
            }}), CreateNewNotesCommandResult.class);
        } catch (DataIntegrityViolationException ignored) {
        }
        final int expectedSizeOfRepository = 0;
        Assert.assertEquals(noteRepository.findAll().size(), expectedSizeOfRepository);
    }

    @Test
    public void correctSaveNote() {
        final CreateNewNotesCommandResult result = commandDispatcher.execute(new CreateNewNotesCommand(new ArrayList<>() {{
            add("test-note");
        }}), CreateNewNotesCommandResult.class);
        Assert.assertNotNull(result);
        final int expectedSizeOfRepository = 1;
        Assert.assertEquals(noteRepository.findAll().size(), expectedSizeOfRepository);
    }

}
