package pl.mm.jutilities.cqrs.springframework.boot.autoconfigure.test.domain.cqrs.command.newmemoandnote;

import org.springframework.stereotype.Service;
import pl.mm.jutilities.cqrs.command.CommandHandler;
import pl.mm.jutilities.cqrs.springframework.boot.autoconfigure.test.domain.entity.Memo;
import pl.mm.jutilities.cqrs.springframework.boot.autoconfigure.test.domain.entity.Note;
import pl.mm.jutilities.cqrs.springframework.boot.autoconfigure.test.domain.repository.MemoRepository;
import pl.mm.jutilities.cqrs.springframework.boot.autoconfigure.test.domain.repository.NoteRepository;

@Service
public final class NewMemoAndNoteCommandHandler implements CommandHandler<NewMemoAndNoteCommand, NewMemoAndNoteCommandResult> {

    private final NoteRepository noteRepository;
    private final MemoRepository memoRepository;

    public NewMemoAndNoteCommandHandler(NoteRepository noteRepository, MemoRepository memoRepository) {
        this.noteRepository = noteRepository;
        this.memoRepository = memoRepository;
    }

    @Override
    public NewMemoAndNoteCommandResult execute(NewMemoAndNoteCommand command) {
        Memo memo = new Memo(command.getMemo());
        Note note = new Note(command.getNote());
        memoRepository.save(memo);
        noteRepository.save(note);
        return new NewMemoAndNoteCommandResult("Memo and note created");
    }

    @Override
    public Class<NewMemoAndNoteCommand> getEventType() {
        return NewMemoAndNoteCommand.class;
    }

    @Override
    public Class<NewMemoAndNoteCommandResult> getEventResultType() {
        return NewMemoAndNoteCommandResult.class;
    }
}
