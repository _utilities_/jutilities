package pl.mm.jutilities.cqrs.springframework.boot.autoconfigure.test.domain.cqrs.command.newmemoandnote;

import pl.mm.jutilities.cqrs.command.Command;

public final class NewMemoAndNoteCommand implements Command {

    private final String note;
    private final String memo;

    public NewMemoAndNoteCommand(String note, String memo) {
        this.note = note;
        this.memo = memo;
    }

    public String getNote() {
        return note;
    }

    public String getMemo() {
        return memo;
    }
}
