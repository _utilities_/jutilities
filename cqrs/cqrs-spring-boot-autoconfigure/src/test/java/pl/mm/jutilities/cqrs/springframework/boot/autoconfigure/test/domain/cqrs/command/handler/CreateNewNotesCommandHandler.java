package pl.mm.jutilities.cqrs.springframework.boot.autoconfigure.test.domain.cqrs.command.handler;

import org.springframework.stereotype.Service;
import pl.mm.jutilities.cqrs.command.CommandHandler;
import pl.mm.jutilities.cqrs.springframework.boot.autoconfigure.test.domain.cqrs.command.CreateNewNotesCommand;
import pl.mm.jutilities.cqrs.springframework.boot.autoconfigure.test.domain.cqrs.command.result.CreateNewNotesCommandResult;
import pl.mm.jutilities.cqrs.springframework.boot.autoconfigure.test.domain.entity.Note;
import pl.mm.jutilities.cqrs.springframework.boot.autoconfigure.test.domain.repository.NoteRepository;

@Service
public final class CreateNewNotesCommandHandler implements CommandHandler<CreateNewNotesCommand, CreateNewNotesCommandResult> {

    private final NoteRepository noteRepository;

    public CreateNewNotesCommandHandler(NoteRepository noteRepository) {
        this.noteRepository = noteRepository;
    }

    @Override
    public CreateNewNotesCommandResult execute(CreateNewNotesCommand command) {
        for (String note : command.getNotes()) {
            final Note noteToSave = new Note(note);
            noteRepository.save(noteToSave);
        }
        return new CreateNewNotesCommandResult();
    }

    @Override
    public Class<CreateNewNotesCommand> getEventType() {
        return CreateNewNotesCommand.class;
    }

    @Override
    public Class<CreateNewNotesCommandResult> getEventResultType() {
        return CreateNewNotesCommandResult.class;
    }
}
