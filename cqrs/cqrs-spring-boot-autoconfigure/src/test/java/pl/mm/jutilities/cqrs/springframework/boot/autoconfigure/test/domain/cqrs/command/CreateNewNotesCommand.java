package pl.mm.jutilities.cqrs.springframework.boot.autoconfigure.test.domain.cqrs.command;

import pl.mm.jutilities.cqrs.command.Command;

import java.util.List;

public final class CreateNewNotesCommand implements Command {

    private final List<String> notes;

    public CreateNewNotesCommand(List<String> notes) {
        this.notes = notes;
    }

    public List<String> getNotes() {
        return notes;
    }
}
