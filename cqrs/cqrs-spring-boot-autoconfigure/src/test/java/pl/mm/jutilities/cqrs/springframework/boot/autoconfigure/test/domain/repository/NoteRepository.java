package pl.mm.jutilities.cqrs.springframework.boot.autoconfigure.test.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.mm.jutilities.cqrs.springframework.boot.autoconfigure.test.domain.entity.Note;

@Repository
public interface NoteRepository extends JpaRepository<Note, Long> {
}
