package pl.mm.jutilities.cqrs.springframework.boot.autoconfigure.test.domain;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@ComponentScan
@EnableJpaRepositories(basePackages = {
        "pl.mm.jutilities.cqrs.springframework.boot.autoconfigure.test.domain.repository"
})
@EntityScan(basePackages = {
        "pl.mm.jutilities.cqrs.springframework.boot.autoconfigure.test.domain.entity"
})
@EnableAutoConfiguration
@Configuration
public class CqrsAutoConfigurationTransactionalHandleTestConfiguration {
}
