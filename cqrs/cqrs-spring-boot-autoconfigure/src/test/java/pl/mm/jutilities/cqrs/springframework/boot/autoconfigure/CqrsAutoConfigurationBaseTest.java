package pl.mm.jutilities.cqrs.springframework.boot.autoconfigure;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;

@ContextConfiguration(classes = CqrsAutoConfiguration.class)
@SpringBootTest
public abstract class CqrsAutoConfigurationBaseTest extends AbstractTestNGSpringContextTests {
}
