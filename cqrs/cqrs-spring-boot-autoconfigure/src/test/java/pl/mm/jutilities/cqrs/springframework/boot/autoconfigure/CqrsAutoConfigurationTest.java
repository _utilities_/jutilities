package pl.mm.jutilities.cqrs.springframework.boot.autoconfigure;

import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.Test;
import pl.mm.jutilities.cqrs.command.dispatcher.CommandDispatcher;
import pl.mm.jutilities.cqrs.query.dispatcher.QueryDispatcher;

public class CqrsAutoConfigurationTest extends CqrsAutoConfigurationBaseTest {

    @Autowired
    private CqrsAutoConfiguration cqrsAutoConfiguration;
    @Autowired
    private QueryDispatcher queryDispatcher;
    @Autowired
    private CommandDispatcher commandDispatcher;

    @Test
    public void checkIfAutoConfigurationInstanceHasBeenCreated() {
        Assert.assertNotNull(cqrsAutoConfiguration);
    }

    @Test
    public void checkIfQueryDispatcherInstanceHasBeenCreated() {
        Assert.assertNotNull(queryDispatcher);
    }

    @Test
    public void checkIfCommandDispatcherInstanceHasBeenCreated() {
        Assert.assertNotNull(commandDispatcher);
    }

}