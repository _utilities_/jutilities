package pl.mm.jutilities.cqrs.springframework.boot.autoconfigure.test.domain.cqrs.command.newmemoandnote;

import pl.mm.jutilities.cqrs.command.CommandResult;

public final class NewMemoAndNoteCommandResult implements CommandResult {

    private final String result;

    public NewMemoAndNoteCommandResult(String result) {
        this.result = result;
    }

    public String getResult() {
        return result;
    }
}
